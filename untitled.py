#!/usr/bin/env python3


def function(parameter):
    return ...


class ClassName:
    static_variable = 0

    def __init__(self):
        self.instance_variable = 0

    def method_name():
        return ...


if __name__ == "__main__":
    ...
