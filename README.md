# [python-template](https://gitlab.com/eidoom/python-template)

Start with `source init.sh` to activate a project virtual environment.

If packages are edited, use `pip freeze > requirements.txt`.
